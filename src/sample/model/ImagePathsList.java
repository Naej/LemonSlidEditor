package sample.model;

import java.util.ArrayList;

public class ImagePathsList {
    private ImagePathsList(){
    }
    private static ImagePathsList images = new ImagePathsList();

    public static ImagePathsList getInstance(){
        return images;
    }



    private ArrayList<String> paths = new ArrayList<>();
    private int currentImage = -1;

    public void initializePaths(ArrayList<String> paths){
        this.paths = paths;
        currentImage = 0;
    }

    public void addImage(String path){
        currentImage++;
        paths.add(currentImage, path);
    }

    public ArrayList<String> getPaths(){
        return paths;
    }

    public int getStatus(){
        if (currentImage < 0){
            return -1;
        }else if(currentImage>=paths.size()){
            return 1;
        }else{
            return 0;
        }
    }
    public String getCurrentImagePath(){
        if (getStatus() != 0 ){
            return null;
        }else{
            return paths.get(currentImage);
        }
    }

    public boolean deleteCurrentImagePath(){
        if(getStatus()==0){
            paths.remove(currentImage);
            currentImage--;
            return true;
        }return false;
    }

    public boolean nextImage(){
        if(getStatus() != 1){
            currentImage++;
            return true;
        }
        return false;
    }
    public boolean prevImage(){
        if(getStatus() != -1){
            currentImage--;
            return true;
        }
        return false;
    }

    @Override
    public String toString(){
        return "IMAGEPATHS \nIndex :" + currentImage + "\n LIST :\n" + paths.toString();
    }
}
