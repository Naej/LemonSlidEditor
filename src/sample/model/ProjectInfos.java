package sample.model;

public class ProjectInfos {
    public String name = "default";
    public String lemonSlidePath = null;

    private ProjectInfos(){
    }
    private static ProjectInfos infos = new ProjectInfos();

    public static ProjectInfos getInstance(){
        return infos;
    }

    public String getFilesPath() {
        if (lemonSlidePath == null){
            return null;
        }else{
            String path = "";
            path = lemonSlidePath.substring(0,lemonSlidePath.lastIndexOf('\\')+1);
            path += "files\\";
            return path;
        }
    }
    public String getProjectPath(){
        return getFilesPath()+name;
    }

    public String getLauncherPath(){
        return lemonSlidePath.substring(0, lemonSlidePath.lastIndexOf('\\'))+"\\" + name + ".html";
    }
}
