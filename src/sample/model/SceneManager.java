package sample.model;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

public class SceneManager {
    private HashMap<String,URL> scenes = new HashMap<>();
    private Stage primaryStage;


    private SceneManager(){
    }
    private static SceneManager manager = new SceneManager();

    public static SceneManager getInstance(){
        return manager;
    }

    public void setStage(Stage primaryStage){
        this.primaryStage = primaryStage;
    }
    public void addScene(String name, URL scene) throws IOException {
        scenes.put(name, scene);
    }
    public void switchScene(String name)  {
        if(scenes.containsKey(name)){
            Parent p = null;
            try {
                p = FXMLLoader.load(scenes.get(name));
                primaryStage.setScene(new Scene(p, 600, 400));
                primaryStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            }
    }
}
