package sample.controller;

import sample.model.ImagePathsList;
import sample.model.ProjectInfos;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

//TODO: improve error management
public class FilesManager {
    public static boolean createProjectFolder(){
        File dir = new File(ProjectInfos.getInstance().getProjectPath());
        if(ProjectInfos.getInstance().name.toLowerCase().equals("lemonslide")){ //because we don't want to break lemonslide.html
            return false;
        }
        // attempt to create the directory here
        return dir.mkdir();

    }


    public static boolean saveImage(String imagePath){
        try{
            Files.copy(Paths.get(imagePath),Paths.get(ProjectInfos.getInstance().getProjectPath()+imagePath.substring(imagePath.lastIndexOf('\\'))));
        } catch (IOException e) {
            System.out.println("Cannot save image :" + imagePath.substring(imagePath.lastIndexOf('\\')) + " it probably already exist.");
            return false;
        }
        return true;
    }

    private static String generateXmlText(ArrayList<String> imagesPaths){
        String xmlText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<lemonslide>\n";
        for(String path : imagesPaths){
            xmlText += "\t<image>"+path.substring(path.lastIndexOf('\\'))+"</image>\n";
        }
        xmlText += "</lemonslide>";
        return xmlText;
    }

    public static boolean generateXml(ArrayList<String> imagePaths){
        Path file = Paths.get(ProjectInfos.getInstance().getProjectPath()+"\\lemonslide.xml");
        try {
            Files.write(file, generateXmlText(imagePaths).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    public static boolean generateLauncher(){
        Path file = Paths.get(ProjectInfos.getInstance().getLauncherPath());

        String projectFileContent =
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                "<title>LemonSlide Demo</title>\n" +
                "</head>\n" +
                "<style>\n" +
                "body{background-color:#eee;}\n" +
                "</style>\n" +
                "\n" +
                "<body>\n" +
                "<p><br/></p>\n" +
                "\n" +
                "<center>\n" +
                "<iframe width=\"600\" height=\"390\" style=\"border:solid 1px #ccc;\" src=\"lemonslide.html?f=" + ProjectInfos.getInstance().name+"\" allowFullScreen=\"true\"></iframe>\n" +
                "</center>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        try {
            Files.write(file,projectFileContent.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean loadProject(ProjectInfos project){
        ArrayList<String> images = loadFXML(project);

        if (images != null){
            ImagePathsList.getInstance().initializePaths(images);
            return true;
        }

        return false;
    }

    private static ArrayList<String> loadFXML(ProjectInfos project){

        System.out.println(project.getProjectPath()+"\\lemonslide.xml");
        Path file = Paths.get(project.getProjectPath()+"\\lemonslide.xml");
        ArrayList<String> imagePaths = new ArrayList<>();

        try {
            List<String> lines = Files.readAllLines(file);
            for(String line : lines){
                if(line.startsWith("\t<image>") && line.endsWith("</image>")){
                    String imageName = line.substring("\t<image>\\".length(),line.indexOf("</image>"));
                    imagePaths.add(project.getProjectPath()+"\\"+imageName);
                }
            }
            return imagePaths;
        } catch (IOException e) {
            return null;
        }
    }
}
