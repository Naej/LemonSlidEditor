package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.text.Text;
import sample.model.ImagePathsList;
import sample.model.ProjectInfos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class EditorController {
    @FXML
    private ImageView myImageView;

    @FXML
    private Text helperText;


    private String lastPath;
    private ImagePathsList images = ImagePathsList.getInstance();


    @FXML
    void drag(DragEvent event) {
        Dragboard board = event.getDragboard();
        if (board.hasFiles()) {

            List<File> phil = board.getFiles();
            String tempPath = phil.get(0).toPath().toString();
            String path = tempPath.toLowerCase();

            if (    path.endsWith(".jpg")  ||
                    path.endsWith(".gif")  ||
                    path.endsWith(".png")  ) {
                lastPath = tempPath;
                event.acceptTransferModes(TransferMode.ANY);
            }
        }
    }
    @FXML
    void drop(DragEvent event) {

            try {
                Dragboard board = event.getDragboard();
                List<File> phil = board.getFiles();
                FileInputStream fis;
                fis = new FileInputStream(phil.get(0));
                myImageView.setImage(new Image(fis));

                images.addImage(lastPath);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


    }



    public void saveProject(){
        FilesManager.createProjectFolder();
        for(String path : images.getPaths()){
            FilesManager.saveImage(path);
        }
        FilesManager.generateXml(images.getPaths());
        FilesManager.generateLauncher();



    }
    public void saveAndPlay(){
        saveProject();
        playProject();
    }
    public void playProject(){
        try {
            java.awt.Desktop.getDesktop().browse(new URI("file:///"+ProjectInfos.getInstance().getLauncherPath().replace('\\','/')));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void deleteCurrentImage() throws FileNotFoundException {
        if(images.deleteCurrentImagePath()){
            refreshImage();
        }
    }

    public void refreshImage(){
        if(images.getStatus() == 0){
            try {
                myImageView.setImage(new Image(new FileInputStream(images.getCurrentImagePath())));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            helperText.setText("drag and drop to add next frame");
        }else{
            myImageView.setImage(null);
            if(images.getStatus() == -1){
                helperText.setText("drag and drop to add the first frame");
            }else{
                helperText.setText("drag and drop to add the last frame");
            }
        }
    }

    public void nextFrame(){
        if(images.nextImage()){
            refreshImage();
        }
    }


    public void prevFrame(){
        if(images.prevImage()){
            refreshImage();
        }
    }

    public void debug(){
        System.out.println(images);
    }

    @FXML
    public void initialize(){
        System.out.println("editor");
        refreshImage();
    }
    //TODO:
    /*
    Menu :
        reset
        new project

        undo/redo
        timeline/move frames
    Load :
        Auto load last project (projectinfos serialization)


    Clarify wrong project name error  if it's because of an existing project
        maybe ask if he wants to load it instead
    Keyboard controls ?
    Viewer mode ?
    deactivate buttons when unusable during the edition
     */
}
