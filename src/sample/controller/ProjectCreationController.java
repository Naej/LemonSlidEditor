package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.text.Text;
import sample.model.ProjectInfos;
import sample.model.SceneManager;

import java.io.File;
import java.util.List;

public class ProjectCreationController{

    private String lastPath;
    private ProjectInfos project = ProjectInfos.getInstance();

    @FXML
    private TextField textfield;

    @FXML
    private Text lemonslidepathtext;

    @FXML
    private Button next;

    @FXML
    private Text errortext;

    @FXML
    void drag(DragEvent event) {
        Dragboard board = event.getDragboard();
        if (board.hasFiles()) {

            List<File> phil = board.getFiles();
            String tempPath = phil.get(0).toPath().toString();
            String path = tempPath.toLowerCase();

            if(path.endsWith(".html")){
                lastPath = tempPath;
                event.acceptTransferModes(TransferMode.ANY);
            }
        }
    }

    @FXML
    void drop(DragEvent event) {
        project.lemonSlidePath = lastPath;
        if(!lastPath.endsWith("lemonslide.html")){
            project.name = lastPath.substring(
                    lastPath.lastIndexOf('\\')+1,
                    lastPath.indexOf(".html"));
            //load project
            if (FilesManager.loadProject(project)){
                SceneManager.getInstance().switchScene("editor");
            }else if(lastPath.endsWith("example.html")){
                project.name = "Example";
                if (FilesManager.loadProject(project)){
                    SceneManager.getInstance().switchScene("editor");
                }

            }
        }
        lemonslidepathtext.setText("Thanks !");
        updateButton();
    }

    public void updateButton(){
        if(textfield.getText() != null && !textfield.getText().equals("") && ProjectInfos.getInstance().lemonSlidePath != null){
            next.setDisable(false);
        }
    }

    @FXML
    void next(){
        project.name = textfield.getText();
        boolean created = FilesManager.createProjectFolder();
        if(created) {
            SceneManager.getInstance().switchScene("editor");
        }else{
            errortext.setText("Incorrect project name");
            System.out.println("Incorrect project name :\n" +
                    "the project probably already exists \n" +
                    "or you used somme illegal characters\n" );
        }
    }

    @FXML
    public void initialize(){
        System.out.println("projectcreator");
    }
}
