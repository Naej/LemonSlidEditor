package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.model.SceneManager;

import java.net.URL;

public class Main extends Application {
    private SceneManager manager;
    @Override
    public void start(Stage primaryStage) throws Exception{
      /*  Parent root = FXMLLoader.load(getClass().getResource("view/editor.fxml"));
        primaryStage.setTitle("LemonSlidEditor");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();*/
        primaryStage.setTitle("LemonSlidEditor");
        manager = SceneManager.getInstance();
        manager.setStage(primaryStage);
        manager.addScene("editor",getClass().getResource("view/editor.fxml")); //Initialize method is called here
        manager.addScene("menu",getClass().getResource("view/projectcreation.fxml")); //Initialize method is called here
        URL mavar = getClass().getResource("view/editor.fxml");
        manager.switchScene("menu");

    }
    //TIP:
    /*
    * if i want to use the initialize method on my controllers,
    * maybe just get the class here and load it on the SceneManager
    */


    public static void main(String[] args) {
        launch(args);
    }
}
